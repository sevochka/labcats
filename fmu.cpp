//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Image1Click(TObject *Sender)
{
	FCountCat++;
	laCountCat->Text = "Cat = " + IntToStr(FCountCat);
	//FloatAnimation1->Start();
	FloatAnimation3->Start();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Image2Click(TObject *Sender)
{
	FCountLuna++;
	laCountLuna->Text = "Moon = " + IntToStr(FCountLuna);
	//FloatAnimation4->Start();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
 FCountCat = 0;
 FCountLuna = 0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormResize(TObject *Sender)
{
	FloatAnimation1->StopValue = this->Width - Image1->Width;
	FloatAnimation1->StopValue = this->Width - Image2->Width;
}
//---------------------------------------------------------------------------
